1
00:00:09,490 --> 00:00:14,130
When we talk about the work of da'wah we often talk about the clarity of the Qur'an and

2
00:00:14,530 --> 00:00:19,289
how perfect it is and how miraculous it is and how amazing it is but that's only half the picture

3
00:00:20,380 --> 00:00:26,580
The clarity of the message and the Perfection of the message is 1/2 the picture. What gave that message its credibility

4
00:00:26,769 --> 00:00:32,459
above and beyond its own perfection is the one that was delivering that message had a character that had credibility

5
00:00:32,860 --> 00:00:34,630
and had a personality that had

6
00:00:34,630 --> 00:00:41,519
credibility. He was really recognized as a symbol of justice and honesty and mercy in the society even before he uttered a

7
00:00:41,520 --> 00:00:48,749
word of Qur'an he already established his credibility. Now think about that how are we going to actually do the job of da'wah

8
00:00:49,360 --> 00:00:56,909
just by talking, making pamphlets about how awesome the Qur'an is and not have any credibility of our own we don't stand for justice

9
00:00:57,699 --> 00:01:05,219
We're not the examples of taking care of the needy or standing up against all forms of injustice whether they affect us or anybody else

10
00:01:05,740 --> 00:01:10,949
We don't go in an uproar when somebody else is oppressed "Oh! That's their problem!"

11
00:01:11,530 --> 00:01:14,280
But if something comes and happens to us we're ready to protest

12
00:01:14,830 --> 00:01:18,479
That's not justice that's self-centered, that's a self-centered attitude and

13
00:01:18,670 --> 00:01:23,729
That's makes us no different than any other lobby group that is interested only in helping who?

14
00:01:24,340 --> 00:01:26,340
Their ownselves, not the cause of justice

15
00:01:26,820 --> 00:01:30,680
But this is something the Messenger alaihissalatu wassalam was far above and

16
00:01:31,120 --> 00:01:34,440
even I mean my teacher when explaining this concept gives some very powerful

17
00:01:34,720 --> 00:01:38,879
examples like you know the family of Yasir(r.a) was being tortured, killed

18
00:01:39,400 --> 00:01:44,399
What did the Messenger(pbuh) tell them? Is there a protest?

19
00:01:44,829 --> 00:01:51,179
"No torturing Muslims" "We're not terrorists" "We're just asking for you to believe in one one God" and

20
00:01:51,430 --> 00:01:56,159
You know "We're not here to hurt anyone" and he's not in no protest. Actually he tells them "Be patient"

21
00:01:56,950 --> 00:02:01,380
He tells it to Yasir and his family to be patient that their reward will be in paradise

22
00:02:01,689 --> 00:02:06,959
But at the same time when a mushrik comes to the Messenger alaihissalatu wassalam and Abu Jahal is taking his money

23
00:02:07,569 --> 00:02:12,449
When he comes to the Messenger... This is a mushrik, coming to the Messenger for help. At a time when

24
00:02:13,170 --> 00:02:17,580
the muslims are being persecuted and so he comes in for help and he says

25
00:02:17,580 --> 00:02:21,959
"Abu Jahal took my money" He grabs him by the hand goes to Abu Jahal and tells him "You better give him his money back"

26
00:02:22,360 --> 00:02:24,360
He starts fighting for the rights of who?

27
00:02:25,080 --> 00:02:31,940
For the mushrik! When the Qur'an came down with ayaat about the baby girl being buried, it wasn't the baby girls of Muslims

28
00:02:33,200 --> 00:02:38,840
These were baby girls have been aborted in non-muslim families and mushrik families. You could say that's their problem but no!

29
00:02:38,840 --> 00:02:43,400
The Qur'an came with a call of justice. Justice was affecting Muslims or not effecting Muslims?

30
00:02:43,810 --> 00:02:50,580
Just as that happened... Whether it dealt with us or not, the Qur'an came with this universal call which also gave it credibility

31
00:02:51,820 --> 00:02:57,989
Download our mobile applications or register on our official website to get access to exclusive content

32
00:02:58,660 --> 00:03:00,660
Links given in description of video

