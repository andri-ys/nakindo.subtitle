﻿1
00:00:00,000 --> 00:00:09,490
(Kartun Islami) Rahmat Kalimat dari "Rahmatan Lil Aalamiin" - Nouman Ali Khan

2
00:00:09,490 --> 00:00:14,130
Ketika berbicara tentang pekerjaan dakwah, seringkali kita berbicara mengenai kejernihan Qur'an

3
00:00:14,530 --> 00:00:19,280
tentang betapa sempurnanya, menakjubkannya, dan mengagumkannya Qur'an itu. Namun, itu hanya setengah dari gambaran utuhnya.

4
00:00:20,380 --> 00:00:26,580
Kejernihan dan kesempurnaan pesan dalam Qur'an hanya separuh dari gambaran utuhnya. Apa yang membuat pesan (Qur'an) tersebut memiliki nilai kredibilitas

5
00:00:26,760 --> 00:00:32,450
yang melampaui kesempurnaan yang melekat padanya berasal dari penyampai pesan tersebut (Rasulullah shallahu'alaihi wassalam), yang memiliki karakter yang kredibel

6
00:00:32,860 --> 00:00:34,630
dan kepribadian terpercaya.

7
00:00:34,630 --> 00:00:41,510
Dia sangat dikenal sebagai simbol dari keadilan, kejujuran, dan belas kasih di masyarakat bahkan sebelum Ia menyampaikan

8
00:00:41,520 --> 00:00:48,740
Al-quran, ia telah membangun kredibilitasnya. Sekarang pikirkanlah, bagaimana kita akan melakukan suatu pekerjaan dakwah

9
00:00:49,360 --> 00:00:55,400
hanya dengan berbicara seputar membuat pamflet yang menunjukkan kehebatan Qur'an dan diri kita sendiri tidak memiliki kredibilitas. 

10
00:00:55,400 --> 00:00:57,000
Kita tidak berdiri untuk keadilan,

11
00:00:57,690 --> 00:01:01,210
Kita bukan contoh dari orang yang peduli terhadap mereka yang membutuhkan, 

12
00:01:01,210 --> 00:01:05,740
atau atau melawan segala bentuk ketidakadilan, terlepas itu berdampak langsung pada kita atau orang lain

13
00:01:05,740 --> 00:01:10,940
Kita tidak merasa terganggu saat orang lain mengalami penindasan. "Oh, itu masalah mereka."

14
00:01:11,530 --> 00:01:14,280
Namun, saat ketidakadilan datang dan terjadi pada diri kita, kita baru protes.

15
00:01:14,830 --> 00:01:18,470
Itu bukan keadilan. Itu sikap egosentris yang membuat kita tidak jauh berbeda dengan para pe-lobby

16
00:01:18,670 --> 00:01:23,720
yang membuat kita tidak jauh berbeda dengan para pe-lobby yang hanya tertarik menolong siapa?

17
00:01:24,340 --> 00:01:26,340
diri mereka sendiri. Bukan atas alasan keadilan.

18
00:01:26,820 --> 00:01:30,680
Namun, Rasululullah Shalallahu 'alaihi wassalam jauh dari hal-hal tersebut

19
00:01:31,120 --> 00:01:34,800
Saat guruku menerangkan tentang konsep ini, ia memberikan beberapa contoh yang sangat kuat

20
00:01:34,800 --> 00:01:38,870
Apakah kamu tahu tentang keluarga Yasir ra? Yasir ra. disiksa dan dibunuh.

21
00:01:39,400 --> 00:01:44,390
Apa yang dikatakan Rasulullah Shalallahu'alaihi wassalam pada keluarganya? Adakah protes?

22
00:01:44,820 --> 00:01:51,170
seperti, "Jangan menyiksa muslim. Kami bukan teroris. Kami hanya meminta kalian untuk percaya pada satu Tuhan.

23
00:01:51,430 --> 00:01:56,150
Kami disini bukan untuk melukai siapapun. Dia tidak protes. Rasulullah Shalallahu 'alaihi wasalam justru meminta mereka untuk bersabar,

24
00:01:56,950 --> 00:02:01,380
meminta keluarga Yasir ra. dan keluarganya untuk bersabar dan mengatakan bahwa ganjaran untuk mereka ada di surga

25
00:02:01,680 --> 00:02:06,950
Di saat yang bersamaan, seorang musyrik mendatangi Rasulullah Shalallahu 'alaihi wasalam karena Abu Jahal mengambil uangnya

26
00:02:07,560 --> 00:02:12,440
Orang ini, seorang musyrik, mendatangi beliau untuk meminta bantuan dan saat itu

27
00:02:13,170 --> 00:02:17,580
kaum Muslim sedang mengalami persekusi. Ia datang meminta bantuan. Ia berkata,

28
00:02:17,580 --> 00:02:21,950
"Abu Jahal mengambil uangku". Beliau SAW menggenggam tangannya, lalu mendatangi Abu Jahal, dan mengatakan " Lebih baik kamu mengembalikan uangnya."

29
00:02:22,360 --> 00:02:24,360
Beliau SAW disini memperjuangkan hak siapa?

30
00:02:25,080 --> 00:02:31,940
seorang musyrik. Saat ayat Al-quran turun dengan ayat tentang bayi perempuan yang dikubur hidup-hidup, bayi tersebut bukan dari keluarga muslim

31
00:02:33,200 --> 00:02:38,840
Tetapi bayi yang digugurkan di keluarga non-muslim dan keluarga musyrik. Kita bisa mengatakan itu masalah mereka, tetapi tidak!

32
00:02:38,840 --> 00:02:43,400
Namun Qur'an sendiri datang sebagai panggilan terhadap keadilan., baik uang berpengaruh terhadap muslim ataupun non muslim

33
00:02:43,810 --> 00:02:50,580
Keadilan yang dihadapkan pada kita atau tidak. Qur'an datang dengan panggilan universal yang menghadirkan kredibilitasnya.

34
00:02:50,580 --> 00:02:55,580
Subtitle : NAK Indonesia 
 Donasi: https://kitabisa.com/nakindonesia

