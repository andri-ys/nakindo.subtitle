﻿1
00:00:10,600 --> 00:00:15,220
When you have a daily relationship 
with the Quran, with understanding

2
00:00:15,220 --> 00:00:17,170
It will change the way you think

3
00:00:17,170 --> 00:00:19,400
It will change the kinds of friends you have

4
00:00:19,400 --> 00:00:22,120
It will change
what you want to do with your spare time

5
00:00:22,120 --> 00:00:24,120
It will make you people of vision

6
00:00:24,120 --> 00:00:27,680
We need young muslims to be people of vision

7
00:00:27,680 --> 00:00:31,600
You need to be people 
that want to change society and make it better

8
00:00:31,600 --> 00:00:35,600
You don't want to be
people of video games, and movies, and sleep

9
00:00:35,600 --> 00:00:37,600
You don't want to be those people

10
00:00:37,600 --> 00:00:42,440
because first comes video games, movies, and sleep
then smoking, then drugs, then alcohol

11
00:00:42,440 --> 00:00:47,680
... and that's life, wasted...
these are lives, hundred of thousands of lives, wasted

12
00:00:47,680 --> 00:00:51,780
not just of non-muslims,
these are problems of muslims youth today

13
00:00:51,780 --> 00:00:53,780
muslims youth have no purpose today

14
00:00:53,780 --> 00:00:55,080
why is that?

15
00:00:55,080 --> 00:00:59,120
because the book that gives us purpose,
we are disconnected from it

16
00:00:59,120 --> 00:01:01,120
We are disconnected from that Book

17
00:01:01,120 --> 00:01:03,520
We have to reconnect with this Book

18
00:01:03,520 --> 00:01:07,480
This is a really serious obligation
we have in the ummah

19
00:01:07,480 --> 00:01:10,180
to help the people come closer to this Book

20
00:01:10,180 --> 00:01:12,180
... and not scare people from this Book

21
00:01:12,180 --> 00:01:15,820
and pushed them away from this Book...
Subhanallah

22
00:01:15,820 --> 00:01:18,400
and you know, when people get angry at the youth

23
00:01:18,400 --> 00:01:21,420
that they are away from the Deen,
I don't get angry at them

24
00:01:21,420 --> 00:01:24,920
I get angry at people like myself...
We haven't done enough

25
00:01:24,920 --> 00:01:27,800
They would not have gone away 
if they knew what this was

26
00:01:27,800 --> 00:01:30,750
and they don't know what this is 
because we're not doing our job

27
00:01:30,750 --> 00:01:33,740
those of us that should be teaching,
should be teaching more...

28
00:01:33,740 --> 00:01:37,560
and those of you that are youth of concern...
become people of concern for others

