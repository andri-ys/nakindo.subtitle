﻿1
00:00:00,000 --> 00:00:10,600
(Kartun Islami) Membangun hubungan dengan Al-Quran – Nouman Ali Khan

2
00:00:10,600 --> 00:00:15,220
Ketika kamu berusaha memahami Al-Qur'an setiap hari

3
00:00:15,220 --> 00:00:17,170
Cara berpikirmu akan berubah

4
00:00:17,170 --> 00:00:19,400
Pergaulanmu juga akan berubah

5
00:00:19,400 --> 00:00:22,120
Demikian juga kegiatanmu di waktu luang

6
00:00:22,120 --> 00:00:24,120
Quran akan menjadikanmu insan yang visioner

7
00:00:24,120 --> 00:00:27,680
Pemuda muslim harus mempunyai visi

8
00:00:27,680 --> 00:00:31,600
Kita harus menjadi agen perubahan di masyarakat
ke arah yang lebih baik

9
00:00:31,600 --> 00:00:35,600
Jangan menjadi pecandu video game, film, dan tidur

10
00:00:35,600 --> 00:00:37,600
Jangan jadi orang seperti itu

11
00:00:37,600 --> 00:00:42,440
Kalau seperti itu
tahap selanjutnya adalah rokok, narkoba, alkohol

12
00:00:42,440 --> 00:00:47,680
Hidupmu terbuang percuma
dan ini dialami juga oleh ratusan bahkan ribuan orang lain

13
00:00:47,680 --> 00:00:51,780
Bukan hanya non-muslim,
pemuda muslim pun banyak yang mengalaminya

14
00:00:51,780 --> 00:00:53,780
Banyak pemuda muslim yang kebingungan

15
00:00:53,780 --> 00:00:55,080
Mengapa bisa demikian?

16
00:00:55,080 --> 00:00:59,120
Meski ada Quran yang bisa menginspirasi kita,
tapi kita jauh darinya

17
00:00:59,120 --> 00:01:01,120
Kita terputus dari Kitab tersebut

18
00:01:01,120 --> 00:01:03,520
Kita harus kembali terhubung dengan Quran

19
00:01:03,520 --> 00:01:07,480
Inilah tanggung jawab besar kita terhadap umat

20
00:01:07,480 --> 00:01:10,180
Untuk membantu umat semakin dekat dengan Al-Quran

21
00:01:10,180 --> 00:01:12,180
Bukan malah membuat mereka takut...

22
00:01:12,180 --> 00:01:15,820
dan semakin menjauhkannya dari Al-Quran... 
Subhanallah

23
00:01:15,820 --> 00:01:18,400
Kalau ada yang marah terhadap para remaja...

24
00:01:18,400 --> 00:01:21,420
karena mereka jauh dari agama...
Saya tidak demikian

25
00:01:21,420 --> 00:01:24,920
Saya marah terhadap diri saya sendiri...
kita harus berbuat lebih

26
00:01:24,920 --> 00:01:27,800
Mereka tak akan seperti itu
kalau mereka paham isinya

27
00:01:27,800 --> 00:01:30,750
Dan mereka demikian
karena kita melalaikan tugas kita...

28
00:01:30,750 --> 00:01:33,740
Kalau Anda bisa mengajar
perbanyaklah jam mengajar Anda

29
00:01:33,740 --> 00:01:39,960
kalau Anda senang membantu orang
semakin giatlah dalam kegiatan Anda

30
00:01:39,960 --> 00:02:10,040
Diterjemahkan oleh : NAK Indonesia (im, ays)
 Donasi: https://kitabisa.com/nakindonesia

